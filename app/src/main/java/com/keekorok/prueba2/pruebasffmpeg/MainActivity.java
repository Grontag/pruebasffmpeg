package com.keekorok.prueba2.pruebasffmpeg;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private File wavFile;
    private File snare;
    private FileOutputStream outputStream;
    private InputStream in;
    private File endFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wavFile= new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/audio.wav");
        //wavFile= new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/carcrash.wav");
        File file=new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/snare.wav");
        endFile=new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/end.wav");
        InputStream in = getResources().openRawResource(R.raw.carcrash);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream =new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int read = 0;
        byte[] bytes = new byte[1024];
        try {
            while ((read = in.read(bytes)) != -1) {
                try {
                    outputStream.write(bytes, 0, read);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!endFile.exists()){
            try {
                endFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FFmpeg ffmpeg = FFmpeg.getInstance(MainActivity.this);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.i("David", "onStart()");
                }

                @Override
                public void onFailure() {
                    Log.i("David", "onFailure()");
                }

                @Override
                public void onSuccess() {
                    Log.i("David", "onSuccess()");
                }

                @Override
                public void onFinish() {
                    Log.i("David", "onFinish()");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
            Log.i("David", "No soportado");
        }

        try {
            Log.i("David", ffmpeg.getDeviceFFmpegVersion());
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }

        //ffmpeg = FFmpeg.getInstance(MainActivity.this);

        String files = "-i " + file.getAbsolutePath() + " -i " + wavFile.getAbsolutePath();
        String filter = "-filter_complex [0:a]volume=0.99[a1];[1:a]volume=0.3[a2][a1][a2];amerge=inputs=2,volume=1.3,pan=stereo|c0<c0+c2|c1<c1+c3[aout]";
        String output = "  -map [aout] -strict -2 "+ endFile.getAbsolutePath();


        String[] cmd=new String[1];
        //String cmdString= files+filter+output;
        //String cmdString="ffmpeg -i "+ file.getAbsolutePath() +" -i "+ wavFile.getAbsolutePath()+ " -filter_complex amix=inputs=2:duration=first:dropout_transition=3 -c:a pcm_s16le -vn -dn -sn -strict -2 " +endFile.getAbsolutePath();
        String cmdString="ffmpeg -i "+ file.getAbsolutePath()+" -i "+wavFile.getAbsolutePath()+" -filter_complex amerge "+ endFile.getAbsolutePath();
        cmd[0]=cmdString;
        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.i("David", "Segundo onStart()");
                }

                @Override
                public void onProgress(String message) {
                    Log.i("David", "Segundo onProgress(): "+message);
                }

                @Override
                public void onFailure(String message) {
                    Log.i("David", "Segundo onFailure()"+ message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.i("David", "Segundo onSuccess(): "+message);
                }

                @Override
                public void onFinish() {
                    Log.i("David", "Segundo onFinish()");
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            Log.i("David", "Error en el execute: ");
            e.printStackTrace();
        }
    }
}
